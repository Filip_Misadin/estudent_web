from django.shortcuts import render

from .models import Article

# Create your views here.
def articles_view(request):
    query_set = Article.objects.all()
    context = {
        "articles": query_set
    }
    #print(request.user)
    return render(request, "articles.html", context)
