from django.db import models

# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=100, blank=False)
    body = models.TextField(blank=False)
    date = models.DateField(auto_now=True, blank=False)
    summary = models.TextField(default='Test')

    def __str__(self):
        return self.title

