from django.db import models

# Create your models here.
class Users(models.Model):
    user_type_id = models.IntegerField()
    privilege_marker = models.CharField(max_length=45, blank=True, null=True)
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    estudent_email = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=50)
    last_login = models.DateTimeField(blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    deleted = models.IntegerField()
    role_group_id = models.CharField(max_length=15, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'users'


class SocialAuthUsersocialauth(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    provider = models.CharField(max_length=32, blank=True, null=True)
    uid = models.CharField(max_length=255, blank=True, null=True)
    extra_data = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'social_auth_usersocialauth'
        
