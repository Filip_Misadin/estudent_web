from django.contrib.auth import logout
from social_core.pipeline.partial import partial

from django.contrib.auth import logout as login

@partial
def require_email(strategy, details, user=None, is_new=False, *args, **kwargs):
    if kwargs.get('ajax') or user and user.email:
        return
    elif is_new and not details.get('email'):
        email = strategy.request_data().get('email')
        if email:
            details['email'] = email
        else:
            current_partial = kwargs.get('current_partial')
            return strategy.redirect(
                '/email?partial_token={0}'.format(current_partial.token)
            )

def social_user(backend, uid, user=None, *args, **kwargs):
    '''OVERRIDED: It will logout the current user
    instead of raise an exception '''

    provider = backend.name
    social = backend.strategy.storage.user.get_social_auth(provider, uid)
    print('dasssssssssaaaaaaaaaaaaaaaaaaaaaaaaaaa')
    if social:
        if user and social.user == user:
            logout(backend.strategy.request)
            #msg = 'This {0} account is already in use.'.format(provider)
            #raise AuthAlreadyAssociated(backend, msg)
        else:
            logout(backend.strategy.request)
            user = social.user    
    
    print(social)
    print(user)
    print(user is None)
    return {'social': social,
            'user': user,
            'is_new': user is None,
            'new_association': False}

def do_something(backend, user, response, *args, **kwargs):
    print(backend)