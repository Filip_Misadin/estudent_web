from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from .models import Users
import json

from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout, login

from social_core.backends.oauth import BaseOAuth1, BaseOAuth2
from social_core.backends.google import GooglePlusAuth
from social_core.backends.utils import load_backends
from social_django.utils import psa, load_strategy

from .decorators import render_to

def logout(request):
    """Logs out user"""
    auth_logout(request)
    return redirect('/profile')


@render_to('home.html')
def home(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated:
        return redirect('done')

@login_required
@render_to('home.html')
def done(request):
    """Login complete view, displays user data"""
    pass

@render_to('home.html')
def validation_sent(request):
    """Email validation sent confirmation page"""
    return {
        'validation_sent': True,
        'email': request.session.get('email_validation_address')
    }


@render_to('home.html')
def require_email(request):
    """Email required page"""
    strategy = load_strategy()
    partial_token = request.GET.get('partial_token')
    partial = strategy.partial_load(partial_token)
    return {
        'email_required': True,
        'partial_backend_name': partial.backend,
        'partial_token': partial_token
    }

@psa('social:complete')
def ajax_auth(request, backend):
    """AJAX authentication endpoint"""
    if isinstance(request.backend, BaseOAuth1):
        token = {
            'oauth_token': request.REQUEST.get('access_token'),
            'oauth_token_secret': request.REQUEST.get('access_token_secret'),
        }
    elif isinstance(request.backend, BaseOAuth2):
        token = request.REQUEST.get('access_token')
    else:
        raise HttpResponseBadRequest('Wrong backend type')
    user = request.backend.do_auth(token, ajax=True)
    login(request, user)
    data = {'id': user.id, 'username': user.username, 'uid': user.uid}
    return HttpResponse(json.dumps(data), mimetype='application/json')

# Create your views here.
def profile_view(request):
    if request.user == 'admin':
        logout(request)

    #print(request.user.email)
    #obj = get_object_or_404(Users, estudent_email=request.user.email)

    if request.user.is_anonymous:
        logged = False
    else:
        logged = request.user.social_auth.exists()

    obj = request.user
    if logged:
        obj = get_object_or_404(Users, estudent_email= request.user.email)

    context = {
        "object": obj,       
        "logged" : logged
    }
    #print(request.user)
    return render(request, "profile.html", context)

def test(request):
    print(request.user.email)
    #print(request.user)
    return render(request, "test.html", {})
